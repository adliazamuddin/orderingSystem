<?php
session_start();
require('fpdf181/fpdf.php');
$con=mysqli_connect('localhost','root','');
mysqli_select_db($con,'db_nesupply');

if(isset($_GET['cust_username']))
{
    $cust_username=$_GET['cust_username'];
	$order_details_id = $_GET['order_details_id'];
	$current_date= date('d-m-Y');;
}

class PDF extends FPDF {
   function Header (){
	   $this->SetFont('Arial', 'B', 15);
	   
	   //dummy cell to put logo
	   //$this->Cell(12,0,'',0,0);
	   //is equivalent to:
	   $this->Cell(12);
	   
	   //put logo
	   $this->Image('NE-logo.png', 12,12,12);
	   
	   //$this->Cell(100,10, 'NOBAT ENGINEERING'0,1);
	   
	  //dummy cell to put logo
	   //$this->Cell(0,5,'',0,1);
	   //is equivalent to:
	   $this->Ln(12);
   }
   function Footer(){
	   // go to 1.5cm from bottom
	   $this->SetY(-15);
	   
	   $this->SetFont('Arial','',8);
	   
	   //width = 0 means the cell is extended up to the right margin
	   $this->Cell(0,10,'Page'.$this->PageNo(), 0,0,'C');
   }
}

//A4 width : 219mm
//default margin : 10mm each sie
//writeable horizontal : 219.(10*2)=189mm

$pdf = new FPDF ('P','mm','A4');
$pdf->AddPage();


$pdf->Image('NE-logo.png',10,10,189);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);


$pdf->SetFont('Arial','B',20);


$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, 'INVOICE',0,1);
$pdf->Cell(80 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->SetFont('Arial','B',14);

$pdf->Cell(130 ,5, 'NOBAT ENGINEERING',0,0);
$pdf->Cell(59  ,5, 'DATE' , 0,1); //end of line

$pdf ->SetFont('Arial','',12);

$pdf->Cell(130 ,5, 'NO  1017 BLOK D TAMAN PUTRA ',0,0);
$pdf->Cell(34 ,5, $current_date, 0,1); //end of line

$pdf->Cell(130 ,5, 'DAMAI, LEMBAH SUBANG, 47301 ',0,1);

$pdf->Cell(130 ,5, 'PETALING JAYA, SELANGOR ',0,1);

$pdf->Cell(130 ,5, 'Phone [03-7831 1662]',0,1);
$pdf->Cell(130 ,5, 'H/P No [019-2025322]',0,1);
$pdf->Cell(59  ,5, '' , 0,1); //end of line


//make a dummy empty cell as a vertical spacer
$pdf->Cell(189 ,10, '',0,1);//end of line

//billing address
$pdf->Cell(100 ,5, 'Bill to',0,1);//end of line


//add dummy cell at beginning of each line for indentation
$query=mysqli_query($con,"select * from user where username='$cust_username' ");
while($data=mysqli_fetch_array($query)){
$pdf->Cell(10 ,5, '',0,0);
$pdf->Cell(90 ,5, $data['username'],0,1);

$pdf->Cell(10 ,5, '',0,0);
$pdf->Cell(90 ,5, $data['cpname'],0,1);

$pdf->Cell(10 ,5, '',0,0);
$pdf->Cell(90 ,5,$data['address'],0,1);

$pdf->Cell(10 ,5, '',0,0);
$pdf->Cell(90 ,5, $data['phone'],0,1);	
	
}

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189 ,10, '',0,1);//end of line

//invoice contents
$pdf->SetFont('Arial','B',12);

$pdf->Cell(130 ,5, 'Product Name',1,0,'C');
$pdf->Cell(25 ,5, 'Quantity',1,0,'C');
$pdf->Cell(38 ,5, 'Price',1,1,'C');//end of line

$pdf->SetFont('Arial','B',12);

//number are right-aligned so we give 'R' after new line parameter
$total_price=0;
$query=mysqli_query($con,"SELECT a.product_id AS product_id,a.product_name as product_name,b.quantity AS quantity,b.product_price AS product_price FROM product a LEFT JOIN order_product b ON b.product_id = a.product_id WHERE b.order_details_id='$order_details_id' ");
while($data=mysqli_fetch_array($query)){
$pdf->Cell(130 ,5, $data['product_name'],1,0,'C');
$pdf->Cell(25 ,5, $data['quantity'],1,0,'C');
$pdf->Cell(38 ,5, $data['product_price'],1,1,'R');//end of line

$total_price+=($data['quantity']*$data['product_price']);
}
//summary

$pdf->Cell(130 ,5, '',0,0);
$pdf->Cell(25  ,5, 'Total Price',1,0);
$pdf->Cell(8   ,5, 'RM',1	,0);
$pdf->Cell(30  ,5, $total_price,1,1,'R');//end of line


$pdf->SetFont('Arial','',12);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(5 ,5, '',0,0);
$pdf->Cell(5 ,5, 'This invoice is generated automatically by computer and do not required a signature.',0,1);
$pdf->Cell(10 ,5, '',0,1);


$pdf->SetFont('Arial','B',14);

$pdf->Cell(10 ,5, 'NOTE',0,1);

$pdf->SetFont('Arial','',14);

$pdf->Cell(10 ,5, '1. Shipping process takes 7 - 14 days working hours to delivered.',0,0);
$pdf->Cell(10 ,5, '',0,1);

$pdf->Cell(10 ,5, '2. After the product safely arrived, you need to verified it to our website.',0,0);
$pdf->Cell(10 ,5, '',0,1);

$pdf->Output();
?>