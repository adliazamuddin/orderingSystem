<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}
	?>
	<?php


if(isset($_POST['filter']))
{
    $date1 = $_POST['date1'];
    $date2 = $_POST['date2'];

    $query = "SELECT * FROM `report` WHERE date BETWEEN '$date1' AND '$date2 ' order by report_id desc";
    $search_result = filterTable($query);
    $total_result = filterTable($query);
    
}
else{
    $query = "SELECT * FROM `report` order by report_id desc";
    $search_result = filterTable($query);
    $total_result = filterTable($query);
}


// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>
<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
			  <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
			  <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
        <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
        <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>REPORT</h1>

		  <article id="box">
		     <div class="box-top"><h2>REPORT LIST</h2></div>
			 <div class="box-panel">
                <table id="product-list" style="width:40%;" >
                <tr>
                <td >
                <form action='adminreport.php' method='post' > 
                <h5>Generate Report</h5></td>
                </tr>
                <tr>
                <td><input type="date" name="date1" required><input type="date" name="date2" required></td>
                </tr>
                <tr>
                <td><select name="report_type" required>
                
                  <option>--Select Type-- </option>
                   <option value="report">Sale</option>
                   <option value="report_product">Product Sale</option>
                 </select> </td>
                 </tr>
                 <tr>
                    <td><input type="submit" class="button darkblue" name="report" value="Generate" > </td>
                    </form> 
                </tr>
                </table>
                <br>
                  <table id="product-list">
                      <tr >
                      <th colspan="3" align="right"><form action="" method="post">
                      <input type="date" name="date1"><input type="date" name="date2">
                      <input type="submit" class="button darkblue" name="filter" value="Filter">
                      </form></th>
                      </tr>
                      <tr>
                        <th>Total Order</th>
                        <th>Total Product Ordered</th>
                        <th>Total Price</th>
                      </tr>
                      <tr>
                      <?php
                      $total_order=0;
                      $total_product_ordered=0;
				      $profit=0;
                      while($row = mysqli_fetch_array($total_result))
                        {  $total_product_ordered+=$row['total_quantity'];
                           $profit+=$row['total_price']; 
                           $total_order++;
                         }?>
                         <td><?php echo $total_order; ?></td>
                        <td><?php echo $total_product_ordered; ?></td>
                        <td>RM <?php echo $profit; ?></td>
                      </tr>
                    </table>
                   
                   <br>

			    <table id="product-list">
				  <tr>
  					<th>Customer</th>
  					<th>Date</th>
  					<th>Total Product</th>
  					<th>Total Price </th>
				  </tr>
				 <?php				 
	     if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>

				    
				    <input type='hidden' name='cust_username' value='".$row['cust_username']."' >
				    <td> <a href='admincustomer.php?cust_username=".$row['cust_username']."' >".$row['cust_username']."</a> </td>
				

				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <td>  <a href='adminreportproduct.php?order_details_id=".$row['order_details_id']."' >".$row['date']."</a></td>
					<input type='hidden' name='total_quantity' value='".$row['total_quantity']."'>
				    <td>".$row['total_quantity']."</td>

					<input type='hidden' name='total_quantity' value='".$row['total_price']."'>
					<td>RM ".$row['total_price']."</td>
					
				 </tr>";

                    
                     }

                  }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>

				</table>
				

			 </div>
         </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
if(isset($_POST['report'])){
                    $date1 = ($_POST['date1']);
                    $date2 = ($_POST['date2']);
                    $report_type = ($_POST['report_type']);
                     if($report_type =="report")
          {
            echo'<script type="text/javascript">window.location.href ="report.php?date1='.$date1.'&date2='.$date2.'";</script>';
            
          }
           else if($report_type =="report_product")
          {
            echo'<script type="text/javascript">window.location.href ="reportproduct.php?date1='.$date1.'&date2='.$date2.'";</script>';
            
          }
          else 
          {
            
            echo'<script type="text/javascript"> alert("Please Select Report Type");window.location.href = "adminreport.php";</script>';
          }
     }

     ?>