<?php
session_start();
require('fpdf181/fpdf.php');
$con=mysqli_connect('localhost','root','');
mysqli_select_db($con,'db_nesupply');
$current_date=date("Y-m-d");

if(isset($_GET['date1'])){
                    $date1 = ($_GET['date1']);
                    $date2 = ($_GET['date2']);
                }

class PDF extends FPDF {
   function Header (){
	   $this->SetFont('Arial', 'B', 15);
	   
	   //dummy cell to put logo
	   //$this->Cell(12,0,'',0,0);
	   //is equivalent to:
	   $this->Cell(12);
	   
	   //put logo
	   $this->Image('NE-logo.png', 12,12,12);
	   
	   //$this->Cell(100,10, 'NOBAT ENGINEERING'0,1);
	   
	  //dummy cell to put logo
	   //$this->Cell(0,5,'',0,1);
	   //is equivalent to:
	   $this->Ln(12);

   }
   function Footer(){
	   // go to 1.5cm from bottom
	   $this->SetY(-15);
	   
	   $this->SetFont('Arial','',8);
	   
	   //width = 0 means the cell is extended up to the right margin
	   $this->Cell(0,10,'Page'.$this->PageNo(), 0,0,'C');
   }
}

//A4 width : 219mm
//default margin : 10mm each sie
//writeable horizontal : 219.(10*2)=189mm

$pdf = new FPDF ('P','mm','A4');

$pdf->SetAutoPageBreak(true,15);
$pdf->AddPage();

$pdf->Image('NE-logo.png',10,10,189);


$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->Cell(130 ,5, '',0,1);

$pdf->SetFont('Arial','B',20);


$pdf->Cell(65 ,5, '',0,0);
$pdf->Cell(130 ,5, 'REPORT',0,1);
$pdf->Cell(80 ,5, '',0,1);

$pdf->Cell(80 ,5, '',0,0);
$pdf->Cell(130 ,5, '',0,1);

$pdf->SetFont('Arial','B',14);

$pdf->Cell(130 ,5, 'NOBAT ENGINEERING',0,0);
$pdf->Cell(59  ,5, 'DATE' , 0,1); //end of line

$pdf ->SetFont('Arial','',12);

$pdf->Cell(130 ,5, 'NO  1017 BLOK D TAMAN PUTRA ',0,0);
$pdf->Cell(34 ,5, $date1.' until '.$date2, 0,1); //end of line

$pdf->Cell(130 ,5, 'DAMAI, LEMBAH SUBANG, 47301 ',0,1);

$pdf->Cell(130 ,5, 'PETALING JAYA, SELANGOR ',0,1);

$pdf->Cell(130 ,5, 'Phone [03-7831 1662]',0,1);
$pdf->Cell(130 ,5, 'H/P No [019-2025322]',0,1);
$pdf->Cell(59  ,5, '' , 0,1); //end of line

$pdf->SetFont('Arial','B',12);

$pdf->Cell(95 ,5, 'Total Product Ordered',1,0,'C');
$pdf->Cell(95 ,5, 'Total Price',1,1,'C');

$pdf->SetFont('Arial','',12);

$query=mysqli_query($con,"select * from report_product WHERE date BETWEEN '$date1' AND '$date2 ' order by report_product_id DESC");
$total_quantity=0;
$total_price=0;
while($data=mysqli_fetch_array($query)){
	$total_quantity+=$data['quantity'];
	$total_price+=$data['product_price'];
}
$pdf->Cell(95 ,5,$total_quantity ,1,0,'C');
$pdf->Cell(95 ,5, "RM ".$total_price,1,1,'C');

$pdf->Cell(68 ,5, '',0,1);

$pdf->SetFont('Arial','B',12);

$pdf->SetFillColor(180,180,255);
$pdf->SetDrawColor(50,50,100);
$pdf->Cell(40 ,5, 'Product Name',1,0,'C');
$pdf->Cell(40 ,5, 'Product Brand',1,0,'C');
$pdf->Cell(50 ,5, 'Quantity',1,0,'C');
$pdf->Cell(60 ,5, 'Price',1,1,'C');


$pdf->SetFont('Arial','',12);
$pdf->SetDrawColor(50,50,100);

$query=mysqli_query($con,"SELECT a.product_id AS product_id,a.product_name as product_name,a.product_image AS product_image,b.product_brand AS product_brand,b.quantity AS quantity,b.product_price AS product_price,b.min_price AS min_price,b.max_price FROM product a LEFT JOIN report_product b ON b.product_id = a.product_id WHERE date BETWEEN '$date1' AND '$date2 ' order by report_product_id DESC");
while($data=mysqli_fetch_array($query)){
$pdf->Cell(40 ,5, $data['product_name'],1,0,'C');
$pdf->Cell(40 ,5, $data['product_brand'],1,0,'C');
$pdf->Cell(50 ,5, $data['quantity'],1,0,'C');
$pdf->Cell(60 ,5, "RM".$data['product_price'],1,1,'C');

}



$pdf->Output();
?>