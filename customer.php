<?php
    session_start();
    require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}

	?>
<?php
 $cust_username=($_SESSION['username']);	 
if(isset($_POST['filter']))
{
    $date1 = $_POST['date1'];
    $date2 = $_POST['date2'];

    $query = "SELECT * FROM `order_details` WHERE cust_username='$cust_username' AND date BETWEEN '$date1' AND '$date2 ' order by order_details_id desc";
    $search_result = filterTable($query);
    
}
else{
    $query = "SELECT * FROM `order_details` where cust_username='$cust_username' order by order_details_id desc";
    $search_result = filterTable($query);         
}


// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=7">
 </head>
 <body >
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
		      <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
			  <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
			  <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>HOME</h1>
		  <article id="box">
		     <table id="showcase">
			  <tr>
			   <th><img src="image/ordernow.jpg "/> </th>
			 </tr>
             <tr>
			 <form action="order.php" method=""  >
               <th><input type="submit" class="button darkblue" value="GO ORDER" ></th>
			 </form>
			 </tr>
             </table>
		  </article>

		  <article id="box">
		     <div class="box-top"><h2>Previous Order</h2></div>
			 <div class="box-panel">
			    <table id="order-list">
			     <tr>
		            <th colspan="7" align="right"><form action="" method="post">
		            <input type="date" name="date1"><input type="date" name="date2">
		            <input type="submit" class="button darkblue" name="filter" value="Filter">
		            </form></th>
		          </tr>
				  <tr>
					<th>Date</th>
					<th>Total Item</th>
					<th>Quotation</th>
					<th>Invoice</th>
					<th>Receipt</th>
					<th>Shipping</th>
					<th>Status</th>
					<th>Delete</th>
				  </tr>
				  <?php  
	        if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>

				    <input type='hidden' name='date' value='".$row['date']."'>
				    <td>  <a href='orderedproduct.php?order_details_id=".$row['order_details_id']."' >".$row['date']."</a></td>

					<input type='hidden' name='total_quantity' value='".$row['total_quantity']."'>
				    <td>".$row['total_quantity']."</td>

                    								
					<form action='customer.php' method='post' >  
                    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <td><input type='hidden' name='quotation' value='Quotation'> <input type='image' name='quotation' src='image/quotation.png' width='35'></a> </td>
				    </form>	
					
					<form action='customer.php' method='post' >  
					<input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
					<input type='hidden' name='cust_username' value='".$cust_username."'>  
                    <td><input type='hidden' name='invoice' value='Invoice'><input type='image'  src='image/invoice.png' width='25'>  </td>
                    </form>	
					
					<form action='customer.php' method='post' >  
                    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
                    <input type='hidden' name='cust_username' value='".$cust_username."'>  
				    <td><input type='hidden' name='receipt' value='Receipt'><input type='image'  src='image/receipt.png' width='25'>  </td> 
				    </form>	 
					 
					 <form action='customer.php' method='post' >  
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <td><input type='hidden' name='shipping' value='Shipping'><input type='image'  src='image/shipping.png' width='35'> </td>                    
                    </form>	
										
					<td>".$row['status']."</td>
					<form action='customer.php' method='post'>
					<input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
					<td><input type='hidden' name='delete' value='delete' ><input type='image' src='image/delete.png' width='30'  onClick=\"javascript: return confirm('Are you sure you want to delete');\"></td>
					</form>

				  </tr> ";
				   }
		         }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }

		        ?>
				</table>
			 </article>

       </section>

     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
	     if(isset($_POST['delete'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $query = "delete from order_details where order_details_id in ('$order_details_id')";
                    $query_run = mysqli_query($con,$query) ;
                     $query = "delete from order_product where order_details_id in ('$order_details_id')";
                    $query_run = mysqli_query($con,$query) ;
                     if($query_run)
					{
						echo'<script type="text/javascript"> alert("Data Deleted");window.location.href = "customer.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
		 }
         
          if(isset($_POST['quotation'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $status="Waiting For Quotation Confirmation";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                    $query_run = mysqli_query($con,$query) ;
                     if(mysqli_num_rows($query_run)>0)
					{

						echo'<script type="text/javascript">window.location.href ="quotation.php?order_details_id='.$order_details_id.' ";</script>';
						
					}
					else 
					{
						$status="Quotation In Progress";
                        $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                       $query_run = mysqli_query($con,$query) ;
                       if(mysqli_num_rows($query_run)>0)
					    {
					    	echo'<script type="text/javascript"> alert("Quotation In Progress");window.location.href = "customer.php";</script>';
						
					    }
							else 	
						{
							echo'<script type="text/javascript">window.location.href = "customer.php";</script>';
							
						}
						
						
					}
		 }
		 else if(isset($_POST['invoice'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $cust_username = ($_POST['cust_username']);
                    $status="Waiting For Invoice Confirmation";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                    $query_run = mysqli_query($con,$query) ;
                     if(mysqli_num_rows($query_run)>0)
					{
						echo'<script type="text/javascript">window.location.href ="viewinvoice.php?order_details_id='.$order_details_id.'&cust_username='.$cust_username.' ";</script>';
						
					}
					else 
					{
							echo'<script type="text/javascript">window.location.href = "customer.php";</script>';
							
						
						
						
					}
		 }
		 else if(isset($_POST['receipt'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $cust_username = ($_POST['cust_username']);
                    $status="Order Successful,Wait For Shipping Confirmation";
                    $status2="Product Received,Order Completed";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND (status='$status' OR status='$status2')";
                    $query_run = mysqli_query($con,$query) ;
                     if(mysqli_num_rows($query_run)>0)
					{
						echo'<script type="text/javascript">window.location.href ="paymentreceipt.php?order_details_id='.$order_details_id.'&cust_username='.$cust_username.' ";</script>';
						
					}
					else 
					{
						$status="Order Confirmation In Progress";
                        $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                       $query_run = mysqli_query($con,$query) ;
                       if(mysqli_num_rows($query_run)>0)
					    {
					    	echo'<script type="text/javascript"> alert("Order Confirmation In Progress");window.location.href = "customer.php";</script>';
						
					    }
							else 	
						{
							echo'<script type="text/javascript">window.location.href = "customer.php";</script>';
							
						}
						
						
					}
		 }
		 else if(isset($_POST['shipping'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $status="Order Successful,Wait For Shipping Confirmation";
                    $status2="Shipping Fixed,Wait For Shipping Confirmation";
                    $status3="Product Not Received,Shipping Pending";

                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND (status='$status' OR status='$status2' OR status='$status3')";
                    $query_run = mysqli_query($con,$query) ;     

                     if(mysqli_num_rows($query_run)>0)
					{
							
							echo'<script type="text/javascript">window.location.href = "shipping.php?order_details_id='.$order_details_id.'";</script>';
						
					}
					else 
					{
						$status="Order Confirmation In Progress";
                        $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                       $query_run = mysqli_query($con,$query) ;
                       if(mysqli_num_rows($query_run)>0)
					    {
					    	echo'<script type="text/javascript">alert("Order Confirmation In Progress");window.location.href = "customer.php";</script>';
						
					    }
							else 	
						{
							echo'<script type="text/javascript"> window.location.href = "customer.php";</script>';
							
						}
						
							 
					}
                   
		 }

 
	     
		 ?>
