<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}
	?>

<?php  //function to display customer

	if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `user` WHERE CONCAT(`username`) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
else if(isset($_GET['cust_username']))
{
    $valueToSearch = $_GET['cust_username'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `user` WHERE username='$valueToSearch'";
    $search_result = filterTable($query);
    
}

 else {
    $query = "SELECT * FROM `user`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}
?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
          <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
			  <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
			  <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
			  <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
			  <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>Customer</h1>
		  <article id="box">
		    <form  action="admincustomer.php" method="post">
	            <input type="search" name="valueToSearch" placeholder="Search customer" >
	            <input  type="submit" class="button darkblue" name="search" value="Search">
            </form>
              
				  
		     <div class="box-top"><h2>CUSTOMER LIST</h2></div>
			 <div class="box-panel">
			 <form action="admincustomer.php" method="post">
			    <table id="product-list">
				  <tr>
					<th>Username</th>
					<th>Email</th>
					<th>Phone Number</th>
                    <th>Company Name</th>
					<th>Company Reg No</th>
					<th>Company Address</th>
					  <th>Delete</th>
				  </tr>
				 <?php
	        if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
				    <td>".$row['username']."</td>
				    <td>".$row['email']."</td>
					<td>".$row['phone']."</td>
					<td>".$row['cpname']."</td>
					<td>".$row['regno']."</td>
					<td>".$row['address']."</td>
					<form action='admincustomer.php' method='post'>
					<input type='hidden' name='cust_username' value='".$row['username']."'>
					<td><input type='hidden' name='delete' value='delete'><input type='image' src='image/delete.png' width='30' onClick=\"javascript: return confirm('Are you sure you want to delete');\"></td>
					</form>
				 </tr>";
                     }
		         }

				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>
				</table>
			  </form>
			 </div>
         </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
	     if(isset($_POST['delete'])){
                    $cust_username = ($_POST['cust_username']);
                    $query = "delete from user where username in ('$cust_username')";
                    $query_run = mysqli_query($con,$query) ;
                    $date=date('Y-m-d');
                    $time=date('h:i:sa');   
                     $activity="admin deleted customer for ".$cust_username;
                     $query="insert into log values('','$date','$time','$activity')";
                     $query_run=mysqli_query($con,$query);
                     if($query_run)
					{
						echo'<script type="text/javascript"> alert("Customer Deleted");window.location.href = "admincustomer.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Customer are used as foreign key")</script>';
					}
		 }

?>
