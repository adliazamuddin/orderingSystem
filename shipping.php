<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}


?>

<?php

if(isset($_GET['order_details_id']))
{
    $order_details_id = $_GET['order_details_id'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM order_details WHERE order_details_id='$order_details_id'";
    $search_result = filterTable($query);
    	
}
// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
          <ul>
           <form action="php/logout.php" method="post">
        <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
          <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
        <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
        <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>SHIPPING</h1>
		  <article id="box">
		     <div class="box-top"><h2>SHIPPING CONFIRMATION</h2></div>
			 <div class="box-panel">
	   <form action="shipping.php" method="post" enctype="multipart/form-data" >
			    <table id="product-list" >
				  <tr>
      					<th>Confirmation</th>
				  </tr>
				 <?php
	     if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
           <input type='hidden' name='order_details_id' value='".$order_details_id."'>
				   <td><h5>Please verify whether you already receive your product or not receive your product yet.</h5>
           <h5>If you are not making verification before 7-14 days working hours after product ordered.We will consider your product have safely arrived to you.</h5>
           <h5>Thank you for your cooperation.</h5></td>
                    

					
				 </tr>";
                     }

                  }

				 else{
					 echo 'No Data ';
				 }
			 }
	     else{
				echo 'Result Error';
			 }
       ?>
            
				</table>

        <br>      
        <table border="0" width="100%">
         <tr >
          <th align="right"><input type="submit" class="button darkblue" name="received" value="Received" onClick="return confirm('Are you sure you received the product')" >
          </th>
          <th align="left" width="50%"><input type="submit" class="button darkblue" name="notreceived" value="Not Received" onClick="return confirm('Are you sure you are not received the product')" >
          </th></form>
          </tr>
        </table>
       

			 </div>
         </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
if(isset($_POST['received']))
     {
     

      $received="Product Received,Order Completed";
      $order_details_id=($_POST['order_details_id']);
      $date=date('Y-m-d');
      $time=date('h:i:sa');   
      
     $query="UPDATE order_details SET status='$received'  WHERE order_details_id='$order_details_id'"; 

       $query_run=mysqli_query($con,$query);


                  if($query_run)
          {
            echo'<script type="text/javascript"> alert("Product Received");window.location.href = "customer.php";</script>';
          }
          else
          {
            echo'<script type="text/javascript">alert("Error")</script>';
          }


      



    }
    else if(isset($_POST['notreceived']))
     {
     

      $notreceived="Product Not Received,Shipping Pending";
      $order_details_id=($_POST['order_details_id']);
      $date=date('Y-m-d');
      $time=date('h:i:sa');   
      
     $query="UPDATE order_details SET status='$notreceived'  WHERE order_details_id='$order_details_id'"; 

       $query_run=mysqli_query($con,$query);


                  if($query_run)
          {
            echo'<script type="text/javascript"> alert("Product Not Received");window.location.href = "customer.php";</script>';
          }
          else
          {
            echo'<script type="text/javascript">alert("Error")</script>';
          }


      



    }



?>