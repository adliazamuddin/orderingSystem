<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}

	?>
<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
			  <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
			  <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
			  <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
			  <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
		  <article id="box">
		     <div class="box-top"><h2>Add Product</h2></div>
			 <div class="box-panel">
   <form action="addproduct.php" method="post" enctype="multipart/form-data">
     <table id="order-list">
	     <tr align="center">
		     <td colspan="2"><h2>Add Product</h2></td>
		 </tr>
		 <tr>
		     <td >Product Name:</td>
			 <td><input type="text" name="product_name" size="40" required/></td>
		 </tr>
		<tr>
		     <td >Product Image:</td>
			 <input type="hidden" name="size" value="1000000">
			 <td><input type="file" name="product_image" required/></td>
		 </tr>
		  <tr>
		     <td >Range Price:</td>
			 <td>RM <input type="number" name="min_price" placeholder="min" > - RM <input type="number" name="max_price" placeholder='max' required></td>
		 </tr>
		 <tr>
		     <td >Product Type:</td>
			 <td><select name="product_type" required>
			     <option> </option>
			     <?php
                   $query="select * from product_type ";

           $query_run =mysqli_query($con,$query);
                 while($row = mysqli_fetch_array($query_run))
                   {
                echo"
                   <option value='".$row['type_id']."'>".$row['type_name']."</option>
                  ";
                  }
                  ?>				  
                 </select></td>
		 </tr>
		 <tr>
		 <td >Product Brand:</td>
		 <td>
		  <?php
                   $query="select * from brand ";
		          $query_run =mysqli_query($con,$query);
                 while($row = mysqli_fetch_array($query_run))
                   {
                echo"
                   <input type='checkbox' name='product_brand[]' value='".$row['brand_id']."'>".$row['brand_name']."
                  ";
                  }
                  ?></td>	
		 </tr>
		 
		 <tr align="center">
		    <td colspan="2"><input type="submit" class="button darkblue" name="add_product" value="Add Product"/></td>
		 </tr>
	 </table>
    </form>

			 </article>

       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
 <?php
	     if(isset($_POST['add_product']))
		 {
		    $target="img/".basename($_FILES['product_image']['name']);

			$product_name=($_POST['product_name']);
			$product_image="img/".($_FILES['product_image']['name']);
			$product_type=($_POST['product_type']);
			$min_price=($_POST['min_price']);
			$max_price=($_POST['max_price']);




					$query="insert into product values('','$product_name','$product_image','$product_type','$min_price','$max_price')";
					$query_run=mysqli_query($con,$query);

                    $last_product_id = mysqli_insert_id($con);

					if((move_uploaded_file($_FILES['product_image']['tmp_name'],$target)) )
					{
						//echo'<script type="text/javascript"> alert("Image Added");</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Image Error")</script>';
					}
                   
                    $i=0;
                    foreach($_POST['product_brand'] as $val){

                    	  $product_brand=($_POST['product_brand'][$i]);
                          $query="insert into product_brand values('','$last_product_id','$product_brand')";
					      $query_run=mysqli_query($con,$query);
                      
                       $i++;
                    }
                    $date=date('Y-m-d');
                    $time=date('h:i:sa');  
                    $activity="admin added product for ".$product_name;
                    $query="insert into log values('','$date',$time,'$activity')";
					$query_run=mysqli_query($con,$query);
                    if(($query_run))
					{
						echo'<script type="text/javascript"> alert("Product Added");window.location.href = "addproduct.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}

					
				


		 }
		 ?>
