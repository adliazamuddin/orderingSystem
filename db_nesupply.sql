-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2017 at 11:46 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_nesupply`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_name`) VALUES
(3, 'Samsung'),
(4, 'Phillips'),
(5, 'Intel'),
(6, 'Honeywell'),
(7, 'AMD'),
(8, 'Baofeng'),
(9, 'Corsair'),
(10, 'Dell'),
(11, 'Kingston'),
(12, 'Maxpark'),
(13, 'Nvidia'),
(14, 'Sony'),
(15, 'Treny Dai');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `product_id` int(100) NOT NULL,
  `cust_username` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `product_id`, `cust_username`, `brand`, `quantity`) VALUES
(20, 80, 'ahmad', 'Corsair', 1),
(21, 79, 'ahmad', 'Corsair', 2);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `activity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `date`, `time`, `activity`) VALUES
(109, '2017-09-24', '11:20:58', 'admin updated quotation for abu'),
(110, '2017-09-24', '11:21:35', 'admin Proceed order for  abu');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_details_id` int(11) NOT NULL,
  `cust_username` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `total_quantity` int(100) NOT NULL,
  `total_price` int(100) NOT NULL,
  `payment_receipt` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_details_id`, `cust_username`, `date`, `total_quantity`, `total_price`, `payment_receipt`, `status`) VALUES
(35, 'abu', '2017-09-24', 1, 0, '', 'Waiting For Invoice Confirmation'),
(36, 'abu', '2017-09-24', 1, 0, '', 'Waiting For Invoice Confirmation'),
(37, 'abu', '2017-09-24', 1, 15, 'receipt/2.jpg', 'Order Completed'),
(38, 'abu', '2017-09-24', 1, 15, 'receipt/2.jpg', 'Order Successful,Shipping In Progress');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `order_id` int(100) NOT NULL,
  `order_details_id` int(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `product_brand` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `max_price` int(100) NOT NULL,
  `min_price` int(100) NOT NULL,
  `product_price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`order_id`, `order_details_id`, `product_id`, `product_brand`, `quantity`, `max_price`, `min_price`, `product_price`) VALUES
(3217, 26, 47, 'Phillips', 1, 200, 44, 150),
(3218, 27, 47, 'Phillips', 1, 89, 78, 79),
(3222, 30, 48, 'Phillips', 8, 64, 63, 0),
(3223, 31, 79, 'Corsair', 2, 14, 13, 13),
(3224, 32, 80, 'Dell', 1, 16, 15, 15),
(3225, 33, 80, 'Dell', 1, 15, 15, 15),
(3226, 34, 80, 'Dell', 1, 15, 15, 15),
(3227, 35, 80, 'Dell', 1, 16, 15, 15),
(3228, 36, 80, 'Dell', 1, 16, 15, 15),
(3229, 37, 80, 'Dell', 1, 16, 15, 15),
(3230, 38, 80, 'Dell', 1, 16, 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_type` int(100) NOT NULL,
  `min_price` int(100) NOT NULL,
  `max_price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `product_type`, `min_price`, `max_price`) VALUES
(47, 'Bullet CCTV Camera', 'img/Bullet CCTV Camera.jpg', 6, 43, 266),
(48, 'Dome CCTV Camera', 'img/Dome CCTV Camera.png', 2, 63, 299),
(49, 'Wireless CCTV ', 'img/Wireless CCTV Camera.jpg', 2, 88, 622),
(50, 'Fluorescent', 'img/Fluorescent.jpg', 6, 28, 69),
(51, 'Incandescent', 'img/Incandescent.jpg', 6, 17, 156),
(52, 'LED', 'img/Light-emitting diode (LED).jpg', 6, 25, 113),
(53, 'Outdoor Solar', 'img/Outdoor solar.jpg', 6, 38, 136),
(54, 'capacitor', 'img/capacitor.jpg', 7, 8, 35),
(55, 'Fan', 'img/Fan.jpg', 7, 10, 48),
(56, 'GeForce GTX 275', 'img/GeForce_GTX_275_GPU.jpg', 7, 355, 499),
(57, 'Hard Drive', 'img/hard-drive.jpg', 7, 159, 475),
(58, 'Motherboard', 'img/Motherboard.jpg', 7, 265, 569),
(59, 'Processor', 'img/Processor.jpg', 7, 289, 1185),
(60, 'PSU', 'img/Psu.jpg', 7, 135, 579),
(61, 'RAM', 'img/ram.jpg', 7, 189, 350),
(62, 'Resistor', 'img/Resistor.jpg', 7, 11, 25),
(63, 'SSD', 'img/SSD.jpg', 7, 230, 667),
(64, 'Burglar Resistant Safes', 'img/Burglar Resistant Safes.jpg', 9, 132, 530),
(65, 'Environment Resistant Safes', 'img/Environmental Resistant Safes.jpg', 9, 220, 560),
(66, 'Fire Resistant Record Protection Equipment', 'img/Fire Resistant Record Protection Equipment.jpg', 9, 352, 789),
(67, 'Fire Resistant Safes', 'img/Fire Resistant Safes.jpg', 9, 250, 462),
(68, 'Jewelery Safes', 'img/Jewelry Safes.jpg', 9, 280, 550),
(69, 'Smart Safe', 'img/Smart Safe.jpg', 9, 280, 660),
(70, 'Wall Safe', 'img/Wall Safes.png', 9, 370, 680),
(71, 'Automatic Lane BArrier', 'img/Automatic Lane Barrier.jpg', 10, 2580, 4500),
(72, 'Barcode Ticket Dispenser', 'img/Barcode Ticket Dispenser.jpg', 10, 4520, 6600),
(73, 'Coin Note Accptor', 'img/Coin Note Acceptor.jpg', 10, 3500, 50000),
(74, 'Parking Guidance System', 'img/Parking Guidance System.png', 10, 2500, 4000),
(75, 'Smart Pass System', 'img/smart-pass-system.jpg', 10, 4580, 8800),
(76, 'Automotive Cable', 'img/Automotive_Cables_.jpg', 8, 48, 120),
(77, 'Flatcord', 'img/Flatcord Wire.jpg', 8, 8, 25),
(78, 'Pureflex NM Wire', 'img/Pureflex NM Wire.jpg', 8, 28, 55),
(79, 'THW Building Wire', 'img/THW Building Wire.jpg', 8, 12, 25),
(80, 'TXL automotive', 'img/TXLautomotive.JPG', 8, 15, 35);

-- --------------------------------------------------------

--
-- Table structure for table `product_brand`
--

CREATE TABLE `product_brand` (
  `product_brand_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_brand`
--

INSERT INTO `product_brand` (`product_brand_id`, `product_id`, `brand_id`) VALUES
(23, 40, 1),
(24, 40, 2),
(25, 41, 1),
(26, 41, 2),
(27, 42, 1),
(28, 42, 2),
(29, 43, 1),
(30, 44, 1),
(31, 45, 14),
(32, 46, 4),
(33, 46, 14),
(34, 47, 4),
(35, 47, 14),
(36, 48, 4),
(37, 48, 14),
(38, 49, 4),
(39, 49, 14),
(40, 50, 4),
(41, 51, 4),
(42, 52, 4),
(43, 53, 4),
(44, 54, 6),
(45, 54, 9),
(46, 54, 10),
(47, 55, 6),
(48, 55, 9),
(49, 55, 10),
(50, 56, 7),
(51, 56, 13),
(52, 57, 3),
(53, 57, 4),
(54, 57, 5),
(55, 57, 9),
(56, 57, 10),
(57, 58, 3),
(58, 58, 5),
(59, 58, 9),
(60, 58, 10),
(61, 59, 5),
(62, 59, 7),
(63, 59, 10),
(64, 60, 9),
(65, 61, 3),
(66, 61, 9),
(67, 62, 9),
(68, 63, 3),
(69, 63, 11),
(70, 63, 14),
(71, 64, 15),
(72, 65, 15),
(73, 66, 15),
(74, 67, 15),
(75, 68, 15),
(76, 69, 15),
(77, 70, 15),
(78, 71, 12),
(79, 72, 12),
(80, 73, 15),
(81, 74, 12),
(82, 75, 12),
(83, 76, 9),
(84, 76, 15),
(85, 77, 9),
(86, 77, 15),
(87, 78, 9),
(88, 78, 15),
(89, 79, 9),
(90, 79, 15),
(95, 80, 10),
(96, 80, 14);

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`type_id`, `type_name`) VALUES
(2, 'CCTV'),
(6, 'Bulb'),
(7, 'Computer Hardware'),
(8, 'Wire'),
(9, 'Safe'),
(10, 'Parking System');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `order_details_id` int(100) NOT NULL,
  `cust_username` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `total_quantity` int(100) NOT NULL,
  `total_price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`report_id`, `order_details_id`, `cust_username`, `date`, `total_quantity`, `total_price`) VALUES
(16, 38, 'abu', '2017-09-24', 1, 15);

-- --------------------------------------------------------

--
-- Table structure for table `report_product`
--

CREATE TABLE `report_product` (
  `report_product_id` int(11) NOT NULL,
  `order_details_id` int(100) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(100) NOT NULL,
  `product_brand` varchar(255) NOT NULL,
  `quantity` int(100) NOT NULL,
  `max_price` int(100) NOT NULL,
  `min_price` int(100) NOT NULL,
  `product_price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_product`
--

INSERT INTO `report_product` (`report_product_id`, `order_details_id`, `date`, `product_id`, `product_brand`, `quantity`, `max_price`, `min_price`, `product_price`) VALUES
(6, 38, '2017-09-24', 80, 'Dell', 1, 16, 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(50) NOT NULL,
  `cpname` varchar(255) NOT NULL,
  `regno` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `email`, `phone`, `cpname`, `regno`, `address`) VALUES
('abu', '123', 'abc@gmail.com', 123456789, 'AxW sdn bhd', '105015-Z', 'No 79 Jalan Nilam 1/8,Taman Teknologi Tinggi Subang,31400  ipoh,Perak'),
('ahmad', '123', 'azw@gmail.com', 123456789, 'AZW sdn bhd', '105015-W', 'No 79 Jalan Nilam 1/8,Taman Teknologi Tinggi Subang,12345  Shah Alam,Selangor'),
('ali', '123', 'abc@gmail.com', 123456789, 'xx sdn bhd', '105015-W', 'No 79 Jalan Nilam 1/8,Taman Teknologi Tinggi Subang,31400  Shah Alam,Selangor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cust_username` (`cust_username`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`product_brand_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `report_product`
--
ALTER TABLE `report_product`
  ADD PRIMARY KEY (`report_product_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `order_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3231;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `product_brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `report_product`
--
ALTER TABLE `report_product`
  MODIFY `report_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
