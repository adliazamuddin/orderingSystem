<?php
    session_start();
    require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}

	?>
<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet"  type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
		<script src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript">
		$(function(){
			var $select = $(".1-100");
			for (i=1;i<=100;i++){
				$select.append($('<option></option>').val(i).html(i))
			}
		});
		</script>
     </header>
     <div id="container">
       <aside>
         <nav>
		  <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
		      <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
			  <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
			  <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>CART</h1>
		  <article id="box">
		     <div class="box-top"><h2>CART LIST</h2></div>
			 <div class="box-panel">
			 <form action='cart.php' method='post'>
			    <table id="list-cart">
				  <tr>
				    <th>Product</th>
				    <th>Brand</th>
				    <th>Range Price/Unit</th>
				    <th>Quantity</th>
					<th>Delete</th>
				  </tr>
				   <?php
		   $cust_username=($_SESSION['username']);	   
           $query="SELECT a.product_id AS product_id,a.product_name as product_name,a.min_price AS min_price,a.max_price AS max_price,b.cart_id AS cart_id,b.brand AS brand,b.quantity AS quantity,b.cust_username AS cust_username FROM product a LEFT JOIN  cart b ON b.product_id = a.product_id  where b.cust_username='$cust_username' order by cart_id desc";

           $query_run =mysqli_query($con,$query);
	        if($query_run)  {
		      if(mysqli_num_rows($query_run)){
                 while($row = mysqli_fetch_array($query_run))
                   {
             echo"
				  <tr>
				    <input type='hidden' name='cart_id' value='".$row['cart_id']."'>
				    <input type='hidden' name='product_id[]' value='".$row['product_id']."'>
					<input type='hidden' name='product_name[]' value='".$row['product_name']."'>
				    <td>".$row['product_name']."</td>
					<input type='hidden' name='product_brand[]' value='".$row['brand']."'>
				    <td>".$row['brand']."</td>
				    <td><p>Min=".$row['min_price']." - Max=".$row['max_price']."</p><p>RM <input type='number' name='minprice[]' placeholder='min' min='".$row['min_price']."' max='".$row['max_price']."' > - RM <input type='number' name='maxprice[]' placeholder='max' min='".$row['min_price']."' max='".$row['max_price']."' required></p></td>
					<td><select class='1-100'  name='quantity[]' > 
					       <option value='".$row['quantity']."'>".$row['quantity']."</option>
                          
                        </select></td>                                       
					<td><input type='hidden'  ><input type='image' name='delete_cart' value='".$row['cart_id']."' src='image/delete.png' width='30'  onClick=\"javascript: return confirm('Are you sure you want to delete');\" formnovalidate></td>					
				 </tr>";
                     }
		         }
				 else{
					 //echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>

				  <tr>
					<td colspan="3"></td>
				    <td ><input type="submit" class="button darkblue" name="reset_product" value="Reset" onClick=" return confirm('Are you sure you want to reset');" formnovalidate></td>
					<td><input type="submit" class="button darkblue" name="order_product" value="Order" ></td>
				  </tr>

				</table>
			  </form>
			 </div>
         </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php         
	     if(isset($_POST['delete_cart'])){

       
	     	        $cart_id=($_POST['delete_cart']);

                    $query = "delete from cart where cart_id in ('$cart_id')";
                    $query_run = mysqli_query($con,$query) ;
                 
                     if($query_run)
					{
						echo'<script type="text/javascript"> alert("Item Deleted");window.location.href = "cart.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
		 } 	

?>

<?php
        if(isset($_POST['reset_product'])) {
             $query = "delete from cart where cust_username='$cust_username'";
                    $query_run = mysqli_query($con,$query) ;

	     	    if($query_run)
					{
						echo'<script type="text/javascript">alert("Cart Reset");window.location.href = "cart.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
			}
        else if(isset($_POST['order_product'])) {
			$date=date('Y-m-d'); 

			$status="Quotation In Progress";
                   $query="insert into order_details values('','$cust_username','$date','$totalquantity','','','$status')";		
					$query_run=mysqli_query($con,$query); 
					$last_details_id = mysqli_insert_id($con);           
            
            $totalquantity=0;
            $i=0;
            foreach($_POST['product_id'] as $val){
            $product_id=($_POST['product_id'][$i]);
			$product_name=($_POST['product_name'][$i]);
			$product_brand=($_POST['product_brand'][$i]);
			$quantity=($_POST['quantity'][$i]);
			$max_price=($_POST['maxprice'][$i]);
			$min_price=($_POST['minprice'][$i]);



					$query="insert into order_product values('','$last_details_id','$product_id','$product_brand','$quantity','$max_price','$min_price','')";
					
					$query_run=mysqli_query($con,$query);                 
                  
                  $totalquantity+=$quantity;
                  $i++;
                   
                   }

                   $query="UPDATE order_details SET total_quantity='$totalquantity' where order_details_id='$last_details_id'";
					
					$query_run=mysqli_query($con,$query);    
	     	   


               $query = "delete from cart where cust_username='$cust_username'";
                    $query_run = mysqli_query($con,$query) ;

	     	    if($query_run)
					{
						echo'<script type="text/javascript"> alert("Product Ordered");alert("Cart Reset");window.location.href = "cart.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
                    
		 } 	
            
		 
?>