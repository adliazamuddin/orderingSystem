<?php
    session_start();
    require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}

	?>

		<?php

if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    //PAGINATION
    $sql = "SELECT COUNT(product_id) FROM product WHERE CONCAT(`product_name`) LIKE '%".$valueToSearch."%'";
	$query = mysqli_query($con, $sql);
	$row = mysqli_fetch_row($query);
	// Here we have the total row count
	$rows = $row[0];
	// This is the number of results we want displayed per page
	$page_rows = 15;
	// This tells us the page number of our last page
	$last = ceil($rows/$page_rows);
	// This makes sure $last cannot be less than 1
	if($last < 1){
	  $last = 1;
	}
	// Establish the $pagenum variable
	$pagenum = 1;
	// Get pagenum from URL vars if it is present, else it is = 1
	if(isset($_GET['pn'])){
	  $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
	}
	// This makes sure the page number isn't below 1, or more than our $last page
	if ($pagenum < 1) { 
	    $pagenum = 1; 
	} else if ($pagenum > $last) { 
	    $pagenum = $last; 
	}
	// This sets the range of rows to query for the chosen $pagenum
	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    // search in all table columns
    // using concat mysql function
    //QUERY DISPLAY PRODUCT
    $query = "SELECT * FROM `product` WHERE CONCAT(`product_name`) LIKE '%".$valueToSearch."%' ORDER BY product_id DESC $limit";
    $search_result = filterTable($query);
    
}
else if(isset($_POST['search_cat']))
{
   $product_type = $_POST['product_type'];
   //PAGINATION
   $sql = "SELECT COUNT(product_id) FROM product WHERE product_type='$product_type'";
	$query = mysqli_query($con, $sql);
	$row = mysqli_fetch_row($query);
	// Here we have the total row count
	$rows = $row[0];
	// This is the number of results we want displayed per page
	$page_rows = 15;
	// This tells us the page number of our last page
	$last = ceil($rows/$page_rows);
	// This makes sure $last cannot be less than 1
	if($last < 1){
	  $last = 1;
	}
	// Establish the $pagenum variable
	$pagenum = 1;
	// Get pagenum from URL vars if it is present, else it is = 1
	if(isset($_GET['pn'])){
	  $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
	}
	// This makes sure the page number isn't below 1, or more than our $last page
	if ($pagenum < 1) { 
	    $pagenum = 1; 
	} else if ($pagenum > $last) { 
	    $pagenum = $last; 
	}
	// This sets the range of rows to query for the chosen $pagenum
	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    // search in all table columns
    // using concat mysql function
     //QUERY DISPLAY PRODUCT
    $query = "SELECT * FROM `product` WHERE product_type='$product_type' ORDER BY product_id DESC $limit";
    $search_result = filterTable($query);
    
    
}
 else {
 	//PAGINATION
 	$sql = "SELECT COUNT(product_id) FROM product ";
	$query = mysqli_query($con, $sql);
	$row = mysqli_fetch_row($query);
	// Here we have the total row count
	$rows = $row[0];
	// This is the number of results we want displayed per page
	$page_rows = 15;
	// This tells us the page number of our last page
	$last = ceil($rows/$page_rows);
	// This makes sure $last cannot be less than 1
	if($last < 1){
	  $last = 1;
	}
	// Establish the $pagenum variable
	$pagenum = 1;
	// Get pagenum from URL vars if it is present, else it is = 1
	if(isset($_GET['pn'])){
	  $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
	}
	// This makes sure the page number isn't below 1, or more than our $last page
	if ($pagenum < 1) { 
	    $pagenum = 1; 
	} else if ($pagenum > $last) { 
	    $pagenum = $last; 
	}
	// This sets the range of rows to query for the chosen $pagenum
	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
	 //QUERY DISPLAY PRODUCT
    $query = "SELECT * FROM `product` ORDER BY product_id DESC $limit";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>
<?php
$textline2 = "Page <b>$pagenum</b> of <b>$last</b>";
// Establish the $paginationCtrls variable
$paginationCtrls = '';
// If there is more than 1 page worth of results
if($last != 1){
  /* First we check if we are on page one. If we are then we don't need a link to 
     the previous page or the first page so we do nothing. If we aren't then we
     generate links to the first page, and to the previous page. */
  if ($pagenum > 1) {
        $previous = $pagenum - 1;
    $paginationCtrls .= '<button class="button darkblue"><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'">Previous</a> </button>&nbsp; &nbsp; ';
    // Render clickable number links that should appear on the left of the target page number
    for($i = $pagenum-4; $i < $pagenum; $i++){
      if($i > 0){
            $paginationCtrls .= '<button class="button darkblue"><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a> </button>&nbsp; ';
      }
      }
    }
  // Render the target page number, but without it being a link
  $paginationCtrls .= ''.$pagenum.' &nbsp; ';
  // Render clickable number links that should appear on the right of the target page number
  for($i = $pagenum+1; $i <= $last; $i++){
    $paginationCtrls .= '<button class="button darkblue"><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></button> &nbsp; ';
    if($i >= $pagenum+4){
      break;
    }
  }
  // This does the same as above, only checking if we are on the last page, and then generating the "Next"
    if ($pagenum != $last) {
        $next = $pagenum + 1;
        $paginationCtrls .= ' &nbsp; &nbsp;<button class="button darkblue"> <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'">Next</a> </button>';
    }
}
$list = '';

?>
<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css"  href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
		<script src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript">
		$(function(){
			var $select = $(".1-100");
			for (i=1;i<=100;i++){
				$select.append($('<option></option>').val(i).html(i))
			}
		});
		function goConfirm(title,link) {
			if(confirm(title)) {
				window.location = link;
				return true;
			}
			return false;
		}
		</script>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
		      <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
			  <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
			  <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>ORDER</h1>
		  <article id="box">
		     <h5>Type :</h5>
			 <div id="categories-search">
			    <form  action="order.php" method="post">
			    <table>
			    <tr><td><select name="product_type" >
			    <option>--Select Type--</option>
			     <?php
                   $query="select * from product_type ";

           $query_run =mysqli_query($con,$query);
                 while($row = mysqli_fetch_array($query_run))
                   {
                echo"
                   <option value='".$row['type_id']."'>".$row['type_name']."</option>
                  ";
                  }
                  ?>			
                 </select></td>
                 <td><input  type="submit" class="search_cat button darkblue" name="search_cat" value="Filter"> </td></tr>
			    </table>
			                     
                  </form>

                   <form  action="order.php" method="post">
                  <input  type="submit" class="search_product button darkblue" name="search" value="Search">
				  <input type="search" placeholder="search product" name="valueToSearch">
				  </form>
			 </div>
			 <br>
		     <div class="box-top"><h2>PRODUCT</h2></div>
			 <div class="box-panel">
			 
			    <table id="product-list">
			    <th colspan="6"><p><?php echo $textline2; ?></p>
  					<p><?php echo $list; ?></p></th>
				  <tr>
					<th>Image</th>
					<th>Product</th>
					<th>Brand</th>
					<th>Range Price</th>
					<th>Quantity</th>
					<th>Order</th>
				  </tr>
				    <?php
	        if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
				  <form action='order.php?action=add&id='".$row['product_id']."'' method='post'>
				    <input type='hidden' name='product_id' value='".$row['product_id']."'>
					<td><img src='".$row['product_image']."' width='150' height='100'></td>
					<input type='hidden' name='product_name' value='".$row['product_name']."'>
				    <td>".$row['product_name']."</td> "?>

					 <td><select name="product_brand" >
					 <option value="-">-</option>
                     <?php
                 $product_id=$row['product_id'];
                $query="SELECT product_brand.*,brand.* from product_brand LEFT JOIN  brand on brand.brand_id=product_brand.brand_id where product_brand.product_id='$product_id' ";
                $query_run =mysqli_query($con,$query);
                 while($row2 = mysqli_fetch_array($query_run))
                   {
				echo"	
                    <option  value='".$row2['brand_name']."'>".$row2['brand_name']."</option>
                    ";
                     }
                      ?>
                      </select></td>
                      <?php
                    echo "  
                    <td>RM ".$row['min_price']." - RM ".$row['max_price']." </td>

					<td><select class='1-100' name='quantity' >
                          
                        </select></td>
					<td><input type='hidden' name='add_cart' value='add_cart'><input type='image' src='image/add.png' width='30'></td>
                    </form>

				 </tr>";
                     }
		         }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>
                <tr>
            		<th colspan="6"><div ><?php echo $paginationCtrls; ?></div></th>
          		 </tr>
				</table>
			 </div></article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
	     if(isset($_POST['add_cart']))
		 {
            $product_id=($_POST['product_id']);
            $cust_username=($_SESSION['username']);
			$product_brand=($_POST['product_brand']);
			$quantity=($_POST['quantity']);

                   $query="select * from cart WHERE product_id='$product_id' AND brand='$product_brand' AND cust_username='$cust_username'";
				   $query_run2=mysqli_query($con,$query);

				if(mysqli_num_rows($query_run2)>0){
					while($row2 = mysqli_fetch_array($query_run2)){
						$quantity2=$row2['quantity'];
						$newquantity=$quantity+$quantity2;
					}
                      $query="update cart set quantity='$newquantity' where product_id='$product_id' AND brand='$product_brand'";
					  $query_run=mysqli_query($con,$query);
					if(($query_run))
					{
						echo'<script type="text/javascript">goConfirm("Added To Cart,Do you want to go cart?","cart.php");</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
                   }
                   else{
					$query="insert into cart values('','$product_id','$cust_username','$product_brand','$quantity')";
					$query_run=mysqli_query($con,$query);


					if(($query_run))
					{
						echo'<script type="text/javascript">goConfirm("Added To Cart,Do you want to go cart?","cart.php");</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}


                   }


		 }
		 ?>