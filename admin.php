<?php
    session_start();
    require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}
	?>
	<?php
  
if(isset($_POST['filter']))
{
    $date1 = $_POST['date1'];
    $date2 = $_POST['date2'];

    $query = "SELECT * FROM `order_details` WHERE date BETWEEN '$date1' AND '$date2 ' order by order_details_id desc";
    $search_result = filterTable($query);
    
}
else{
    $query = "SELECT * FROM `order_details` order by order_details_id desc";
    $search_result = filterTable($query);         
}


// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=4">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
			  <li><a href="admin.php"><img src="image/order.png" width="20" height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20" height="20"> PRODUCT</a></li>
			  <li><a href="admincustomer.php"><img src="image/account.png" width="20" height="20"> CUSTOMER</a></li>
			  <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
			  <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>ORDER</h1>
		  <article id="box">
		     <div class="box-top"><h2>ORDER LIST</h2></div>
			 <div class="box-panel">
			    <table id="order-list">
			      <tr>
		            <th colspan="9" align="right"><form action="" method="post">
		            <input type="date" name="date1"><input type="date" name="date2">
		            <input type="submit" class="button darkblue" name="filter" value="Filter">
		            </form></th>
		          </tr>
				  <tr>
					<th>Customer</th>
					<th>Date</th>
					<th>Total Item</th>
					<th>Quotation</th>
					<th>Invoice </th>
					<th>Payment	</th>					
					<th>Shipping</th>
					<th>Status  </th>
					<th>Deleted </th>
				  </tr>
				  <?php

	        if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>

				    
				    <input type='hidden' name='cust_username' value='".$row['cust_username']."' >
				    <td> <a href='admincustomer.php?cust_username=".$row['cust_username']."' >".$row['cust_username']."</a> </td>
								    
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <td>  <a href='adminorderedproduct.php?order_details_id=".$row['order_details_id']."' >".$row['date']."</a></td>
					<input type='hidden' name='total_quantity' value='".$row['total_quantity']."'>
				    <td>".$row['total_quantity']."</td> 
			
                     <form action='admin.php' method='post' >  
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
                    <td><input type='hidden' name='quotation' value='Quotation'> <input type='image' name='quotation' src='image/quotation.png' width='35'> </td>
                    </form>

                    <form action='admin.php' method='post' >  
					<input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
					<input type='hidden' name='cust_username' value='".$row['cust_username']."'>  
                    <td><input type='hidden' name='invoice' value='Invoice'><input type='image'  src='image/invoice.png' width='25'> </td>
                    </form>	
					
					 <form action='admin.php' method='post' >  
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <input type='hidden' name='cust_username' value='".$row['cust_username']."'> 
                    <td><input type='hidden' name='slip' value='Slip'><input type='image'  src='image/receipt.png' width='25'> </td>
                    </form>	

                    <form action='admin.php' method='post' >  
				    <input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
				    <td><input type='hidden' name='shipping' value='Shipping'><input type='image'  src='image/shipping.png' width='35'> </td>                    
                    </form>	
					

					<td>".$row['status']."</td>
					<form action='admin.php' method='post'>
					<input type='hidden' name='order_details_id' value='".$row['order_details_id']."'>
					<td><input type='hidden' name='cust_username' value='".$row['cust_username']."' >
					<input type='hidden' name='delete' value='delete'><input type='image' src='image/delete.png' width='30' onClick=\"javascript: return confirm('Are you sure you want to delete');\"></td>
					</form>

				  </tr> ";
				   }
		         }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }

		        ?>
				 
				</table>
				</div>
			 </article>

       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
	     if(isset($_POST['delete'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $cust_username = ($_POST['cust_username']);
                    $query = "delete from order_details where order_details_id in ('$order_details_id')";
                    $query_run = mysqli_query($con,$query) ;
                    $query = "delete from order_product where order_details_id in ('$order_details_id')";
                    $query_run = mysqli_query($con,$query) ;
                    $date=date('Y-m-d');
                    $time=date('h:i:sa');   
                     $activity="admin deleted a order for  ".$cust_username;
                     $query="insert into log values('','$date','$time','$activity')";
                     $query_run=mysqli_query($con,$query);
                     if($query_run)
					{
						echo'<script type="text/javascript"> alert("Data Deleted");window.location.href = "admin.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
		 }
		 else if(isset($_POST['quotation'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $status="Quotation In Progress";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                    $query_run = mysqli_query($con,$query) ;
                     if(mysqli_num_rows($query_run)>0)
					{
						echo'<script type="text/javascript">window.location.href ="adminquotation.php?order_details_id='.$order_details_id.'";</script>';	
						
					}
					else 
					{
						echo'<script type="text/javascript"> window.location.href = "admin.php";</script>';
						
					}
		 }
		else if(isset($_POST['invoice'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $cust_username = ($_POST['cust_username']);
                    $status="Waiting For Invoice Confirmation";
                    $status2="Order Successful,Wait For Shipping Confirmation";
                    $status3="Product Received,Order Completed";
                    $status4="Product Not Received,Shipping Pending";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND (status='$status'  OR status='$status2' OR status='$status3' OR status='$status4')";
                    $query_run = mysqli_query($con,$query) ;
                     if(mysqli_num_rows($query_run)>0)
					{
						echo'<script type="text/javascript">window.location.href ="invoice.php?order_details_id='.$order_details_id.'&cust_username='.$cust_username.' ";</script>';
						
					}
					else 
					{
						$status="Waiting For Quotation Confirmation";
                        $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                       $query_run = mysqli_query($con,$query) ;
                       if(mysqli_num_rows($query_run)>0)
					    {
					    	echo'<script type="text/javascript"> alert("Waiting For Quotation Confirmation");window.location.href = "admin.php";</script>';
						
					    }
							else 	
						{
							echo'<script type="text/javascript">window.location.href = "admin.php";</script>';
							
						}
						
						
					}
		 }
		 		else if(isset($_POST['slip'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $status="Order Confirmation In Progress";
                    $status2="Order Successful,Wait For Shipping Confirmation";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND (status='$status' OR status='$status2') ";
                    $query_run = mysqli_query($con,$query) ;     

                     if(mysqli_num_rows($query_run)>0)
					{
						echo'<script type="text/javascript">window.location.href ="adminreceipt.php?order_details_id='.$order_details_id.'";</script>';
						
					}
					else 
					{
						
						$status="Waiting For Invoice Confirmation";
                        $query = "select * from order_details WHERE order_details_id='$order_details_id' AND status='$status' ";
                       $query_run = mysqli_query($con,$query) ;
                       if(mysqli_num_rows($query_run)>0)
					    {
					    	echo'<script type="text/javascript"> alert("Wait For Order Confirmation");window.location.href = "admin.php";</script>';
						
					    }
							else 	
						{
							echo'<script type="text/javascript">window.location.href = "admin.php";</script>';
							
						}

					}
		 }
		 else if(isset($_POST['shipping'])){
                    $order_details_id = ($_POST['order_details_id']);
                    $status="Order Successful,Wait For Shipping Confirmation";
                    $status2="Product Not Received,Shipping Pending";
                    $status3="Shipping Fixed,Wait For Shipping Confirmation";
                    $query = "select * from order_details WHERE order_details_id='$order_details_id' AND (status='$status' OR status='$status2' OR status='$status3')";
                    $query_run = mysqli_query($con,$query) ;     

                     if(mysqli_num_rows($query_run)>0)
					{
							
							echo'<script type="text/javascript">window.location.href = "adminshipping.php?order_details_id='.$order_details_id.'";</script>';
						
					}
					else 
					{
						
							echo'<script type="text/javascript"> window.location.href = "admin.php";</script>';
							
						
						
							 
					}
                   
		 }



?>
