<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';	
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}
	
	?>
	
<!DOCTYPE html>				 
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=1">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>      
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
			  <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
			  <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
			  <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
			  <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
			  <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
		  <article id="box">
		     <div class="box-top"><h2>EditProduct</h2></div>
			 <div class="box-panel">
   <form action="editproduct.php" method="post" enctype="multipart/form-data">
     <table id="order-list">
	     <tr align="center">
		     <td colspan="2"><h2>Edit Product</h2></td>
		 </tr>

		 <?php
			if(isset($_POST['edit_product']))
		 {	
           $product_id=($_POST['product_id']);		   
		   
           $query="select * from `product` where product_id='$product_id'";

           $query_run =mysqli_query($con,$query);
	        
                 while($row = mysqli_fetch_array($query_run))
                   {		   	
            echo"
				      <tr>
		     <input type='hidden' name='product_id' value='".$row['product_id']." '/>
		     <td  ><b>Product Name:</b></td>
			 <td><input type='text' name='product_name' value='".$row['product_name']."'' size='40' /></td>
		 </tr>
		<tr>
		     <td >Product Image:</td>
			 <input type='hidden' name='size' value='1000000'>
			 <td><img src='".$row['product_image']."'' width='150' height='100' ><input type='file' name='product_image' value='".$row['product_image']."' /></td>
		 </tr>
		 <tr>
		     <td >Range Price:</td>
			 <td>RM <input type='number' name='min_price' value='".$row['min_price']."'> - RM <input type='number' name='max_price'  value='".$row['max_price']."' required></td>
		 </tr>";

		 $type_id=$row['product_type'];
		 $product_id=$row['product_id'];
		 }
		}
		 ?>
		 <tr>
		 <td >Product Brand:</td>
		 <td>
		  <?php
                   $query="select * from brand ";
		          $query_run =mysqli_query($con,$query);

                 while($row = mysqli_fetch_array($query_run))
                   {
                $brand_id=$row['brand_id'];
                echo"
                   <input type='checkbox' name='product_brand[]' ";  ?> 
                   <?php
                  $query2="SELECT * FROM product_brand WHERE brand_id = '$brand_id' AND product_id='$product_id'";
	              $result2 = mysqli_query($con,$query2);
	              if(mysqli_num_rows($result2) > 0) echo 'checked="checked"';
	              ?>
                 <?php echo "value='".$row['brand_id']."'>".$row['brand_name']."
                  ";
                  
                  ?>
                  
                 <?php } ?>
                  </td>	
		 </tr>
		 
		 <tr>
		     <td >Product Type:</td>
		     <td><select name="product_type" >
		     <?php $query="select * from `product_type` where type_id='$type_id'";

           $query_run =mysqli_query($con,$query);
	        
                 while($row = mysqli_fetch_array($query_run))
                   { ?>			 
			    <option value="<?php echo $row['type_id']?>"><?php echo $row['type_name']?></option>

			    <?php } ?>
			     <?php
                   $query="select * from product_type ";

           $query_run =mysqli_query($con,$query);
                 while($row = mysqli_fetch_array($query_run))
                   {
                echo"
                   <option value='".$row['type_id']."'>".$row['type_name']."</option>
                  ";
                  }
                  ?>				  
                 </select></td>
		 </tr>
		  	  

		 <tr align="center">
		    <td colspan="2"><input type="submit" class="button darkblue" name="edit" value="Edit Product"/></td>
		 </tr>
	 </table>
    </form>

			 </article>

       </section>	   
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
		 	     if(isset($_POST['edit']))
		 {
		    $target="img/".basename($_FILES['product_image']['name']);
			
			$product_id=($_POST['product_id']);
			$product_name=($_POST['product_name']);
			$product_image="img/".($_FILES['product_image']['name']);
			$product_type=($_POST['product_type']);
			$min_price=($_POST['min_price']);
			$max_price=($_POST['max_price']);

			
			
			        if($_FILES['product_image']['name']==''){
                            $query="UPDATE product SET product_name='$product_name',product_type='$product_type',min_price='$min_price',max_price='$max_price' WHERE product_id='$product_id'";
							$query_run=mysqli_query($con,$query);
                            
                            $query="DELETE FROM product_brand WHERE product_id='$product_id'";
							      $query_run=mysqli_query($con,$query);

							$i=0;
		                    foreach($_POST['product_brand'] as $val){

		                    	  $product_brand=($_POST['product_brand'][$i]);
		                          $query="insert into product_brand values('','$product_id','$product_brand')";
							      $query_run=mysqli_query($con,$query);
		                      
		                       $i++;
		                    }

							      $date=date('Y-m-d'); 
							      $time=date('H:i:sa');
		                          $activity="admin edited product named ".$product_name;
		                          $query="insert into log values('','$date','$time','$activity')";
							      $query_run=mysqli_query($con,$query);
							    if(($query_run))
							    {
								    echo'<script type="text/javascript"> alert("Product Edited");window.location.href = "adminproduct.php";</script>';
							    }
							    else
							    {
								     echo'<script type="text/javascript">alert("Error")</script>';
							    }
					        }
			        else{
			        		$query="UPDATE product SET product_name='$product_name',product_image='$product_image',product_type='$product_type',min_price='$min_price',max_price='$max_price' WHERE product_id='$product_id'";
							$query_run=mysqli_query($con,$query);
							
							
							      if((move_uploaded_file($_FILES['product_image']['tmp_name'],$target)) )
							      {
								      echo'<script type="text/javascript"> alert("Image Edited");</script>';
							      }
							      else
							      {
								      echo'<script type="text/javascript">alert("Image Error")</script>';
							      }

                              $query="DELETE FROM product_brand WHERE product_id='$product_id'";
							      $query_run=mysqli_query($con,$query);

							       $i=0;
		                    foreach($_POST['product_brand'] as $val){

		                    	  $product_brand=($_POST['product_brand'][$i]);
		                          $query="insert into product_brand values('','$product_id','$product_brand' WHERE product_id='$product_id')";
							      $query_run=mysqli_query($con,$query);
		                      
		                       $i++;
		                    }

							      $date=date('Y-m-d'); 
							      $time=date('H:i:sa');
		                          $activity="admin edited product for ".$product_name;
		                          $query="insert into log values('','$date','$time','$activity')";
							      $query_run=mysqli_query($con,$query);
							      if(($query_run))
							      {
								      echo'<script type="text/javascript"> alert("Product Edited");window.location.href = "adminproduct.php";</script>';
							      }
							     else
							     {
								     echo'<script type="text/javascript">alert("Error")</script>';
							     }
			        }

				

		 }
		 ?>