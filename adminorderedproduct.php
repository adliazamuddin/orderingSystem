<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}


?>

<?php
if(isset($_GET['order_details_id']))
{
    $order_details_id = $_GET['order_details_id'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT a.product_id AS product_id,a.product_name as product_name,a.product_image AS product_image,b.product_brand AS product_brand,b.quantity AS quantity,b.product_price AS product_price,b.min_price AS min_price,b.max_price FROM product a LEFT JOIN order_product b ON b.product_id = a.product_id WHERE b.order_details_id='$order_details_id'";
    $search_result = filterTable($query);
    	
}
// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}
?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
        <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
        <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
        <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
        <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>ORDERED PRODUCT</h1>
		  <article id="box">
		     <div class="box-top"><h2>PRODUCT</h2></div>
			 <div class="box-panel">
	   
			    <table id="product-list">
				  <tr>
    					<th>Image</th>
    					<th>Product</th>
    					<th>Brand</th>					
              <th>Quantity</th>
              <th>Range Price</th>
              <th>Price</th>
				  </tr>
				 <?php
				 $total_price=0;
	     if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
				    <input type='hidden' name=product_id value='".$row['product_id']."'>
					<td><img src='".$row['product_image']."' width='150' height='100'></td>
					<td>".$row['product_name']."</td>
					<td>".$row['product_brand']."</td>
					<td>".$row['quantity']."</td>
          <td>RM ".$row['min_price']." - RM ".$row['max_price']." </td>
					<td>".$row['product_price']."</td>
                    

					
				 </tr>";
				 $total_price+=(($row['quantity'])*($row['product_price']));
                     }
                  echo "<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Total Price</td>
                  <td colspan='2'>RM
                  ".$total_price."</td>
                  </tr>";
                  }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>

				</table>

			 </div>
         </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
