<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}


?>

<?php

if(isset($_GET['order_details_id']))
{
    $order_details_id = $_GET['order_details_id'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT a.product_id AS product_id,a.product_name as product_name,a.product_image AS product_image,b.product_brand AS product_brand,b.quantity AS quantity,b.product_price AS product_price,b.min_price AS min_price,b.max_price AS max_price  FROM product a LEFT JOIN order_product b ON b.product_id = a.product_id WHERE b.order_details_id='$order_details_id'";
    $search_result = filterTable($query);
    	
}
// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
        <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
          <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
        <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
        <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>QUOTATION</h1>
		  <article id="box">
		     <div class="box-top"><h2>PRODUCT</h2></div>
			 <div class="box-panel">
	   <form action="quotation.php" method="post" enctype="multipart/form-data" >
			    <table id="product-list" >
				  <tr>
      					<th>Image</th>
      					<th>Product</th>
      					<th>Brand</th>					
                <th>Quantity</th>
                <th>Range Price</th>
                <th>Price</th>
				  </tr>
				 <?php
         $cust_username=($_SESSION['username']);
				 $total_price=0;
	     if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>
           <input type='hidden' name='order_details_id' value='".$order_details_id."'>
           <input type='hidden' name='cust_username' value='".$cust_username."'>
				    <input type='hidden' name=product_id value='".$row['product_id']."'>
					<td><img src='".$row['product_image']."' width='150' height='100'></td>
					<td>".$row['product_name']."</td>
					<td>".$row['product_brand']."</td>
					<td>".$row['quantity']."</td>
          <td>RM ".$row['min_price']." - RM ".$row['max_price']." </td>
					<td>".$row['product_price']."</td>
                    

					
				 </tr>";
				 $total_price+=(($row['quantity'])*($row['product_price']));
                     }
                  echo "<tr>
                      <td colspan='3'>Total Price</td>                 
                      <td colspan='3'>RM ".$total_price."</td>
                  </tr>";
                  }

				 else{
					 echo 'No Data ';
				 }
			 }
	     else{
				echo 'Result Error';
			 }
       ?>
            
				</table>
       <br>      
        <table border="0" width="100%">
         <tr >    
          <th align="right"><input type="submit" class="button darkblue"  name="confirm" value="Proceed">
            </th></form>
            <th align="left"  ">
            <form action="quotation.php" method="post">
             <input type="hidden" name="order_details_id" value="<?php echo $order_details_id ?>">
           <input type="submit" class="button darkblue" onClick="return confirm('Are you sure you want to cancel')" name="cancel" value="Cancel"></th>
           </form>
           </th>
         </tr>
        </table>
			 </div>
        </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html><?php
     if(isset($_POST['confirm'])){
      $confirm="Waiting For Invoice Confirmation";
      $order_details_id=($_POST['order_details_id']);
      $cust_username=($_POST['cust_username']);
     $query="UPDATE order_details SET total_price='$total_price',status='$confirm'  WHERE order_details_id='$order_details_id'"; 

       $query_run=mysqli_query($con,$query);
            if($query_run)
          {
             echo'<script type="text/javascript">window.location.href ="viewinvoice.php?order_details_id='.$order_details_id.'&cust_username='.$cust_username.'";</script>';
          }
          else
          {
            echo'<script type="text/javascript">alert("Error")</script>';
          }
          
      }
     else if(isset($_POST['cancel'])){
                   $order_details_id=($_POST['order_details_id']);
                    
                    $query = "DELETE FROM order_details WHERE order_details_id='$order_details_id'";
                    $query_run = mysqli_query($con,$query) ;
                     if($query_run)
          {
            echo'<script type="text/javascript"> alert("Order Cancel");window.location.href = "customer.php";</script>';
          }
          else
          {
            echo'<script type="text/javascript">alert("Error")</script>';
          }
        }
          ?>